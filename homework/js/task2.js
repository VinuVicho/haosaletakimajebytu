// Your code goes here
let str = prompt('Input: ');
let position; 
let lengths;
let two = 2;
if (str.indexOf(' ') >= 0 || str === '') {
    alert('Invalid value');
}
else {
    if (str.length % two === 0) {
        position = str.length / two - 1;
        lengths = two;
    }
    else {
        position = (str.length - 1) / two;
        lengths = 1;
    }
    alert(str.substring(position, position + lengths));
}
